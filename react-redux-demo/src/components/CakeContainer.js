import React from "react";
import { buyCake } from "../redux";
import { connect } from "react-redux";

function CakeContainer(props) {
  console.log('*****',props)
  return (
    <div>
      <h2> No of Cake - {props.numOfCake}</h2>
      <button onClick={props.buyCake}> BUY CAKE </button>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    numOfCake: state.numOfCake
  };
};

const mapDispatchToProp = (dispatch) => {
  return {
    buyCake: () => dispatch(buyCake())
  };
};
export default connect(mapStateToProps, mapDispatchToProp)(CakeContainer);
